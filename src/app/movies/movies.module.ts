import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './movies.component';
import { Routes, RouterModule } from '@angular/router';



const MOVIES_ROUTES: Routes = [
  {
    path: '',
    component: MoviesComponent,
  },
];


@NgModule({
  declarations: [MoviesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(MOVIES_ROUTES)
  ]
})
export class MoviesModule { }
