import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent implements OnInit {
  tabs = [
  {
    title: 'Home',
    route: '/comic',
  },
  {
    title: 'Movies',
    route: '/movies',
  }
]

  constructor() { }

  ngOnInit() {
  }

}
